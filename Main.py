############### ATTR ###########
_name = ""
_gender = ""
_age = ""
_inp = ""

##################################### CAUSES #######################################

def cause_atherosclerotic():
    print("atherosclerotic cause of Cardiovascular Disease: ")
    print("While cardiovascular disease can refer to different heart or blood vessel problems, the term is often used \n"
          "to mean damage to your heart or blood vessels by atherosclerosis (ath-ur-o-skluh-ROE-sis), a buildup of \n"
          "fatty plaques in your arteries. Plaque buildup thickens and stiffens artery walls, which can inhibit blood \n"
          "flow through your arteries to your organs and tissues.")
    print("Atherosclerosis is also the most common cause of cardiovascular disease. It can be caused by correctable \n"
          "problems, such as an unhealthy diet, lack of exercise, being overweight and smoking.")

def cause_arrhythmia():
    print("Arrhythmia is caused because of the following reasons: ")
    print("1. Coronary artery disease", "2. High Blood Pressure", "3. Diabetes", "4. Smoking", "5. Stress",
          "6. Drug Abuse", sep="\n")
    print("In a healthy person with a normal, healthy heart, it's unlikely for a fatal arrhythmia to develop without \n"
          "some outside trigger, such as an electrical shock or the use of illegal drugs. That's primarily because a \n"
          "healthy person's heart is free from any abnormal conditions that cause an arrhythmia, such as an area of \n"
          "scarred tissue.")
    print("However, in a heart that's diseased or deformed, the heart's electrical impulses may not properly start or \n"
          "travel through the heart, making arrhythmias more likely to develop.")

def cause_heartDefects():
    print("Congenital heart defects: ")
    print("Congenital heart defects usually develop while a baby is in the womb. Heart defects can develop as the \n"
          "heart develops, about a month after conception, changing the flow of blood in the heart. Some medical \n"
          "conditions, medications and genes may play a role in causing heart defects")
    print("Heart defects can also develop in adults. As you age, your heart's structure can change, causing a heart \n"
          "defect.")

def cause_cardiomyopathy():
    print("Dilated cardiomyopathy: ")
    print("The cause of this most common type of cardiomyopathy often is unknown. It may be caused by reduced blood \n"
          "flow to the heart (ischemic heart disease) resulting from damage after a heart attack, infections, \n"
          "toxins and certain drugs. It may also be inherited from a parent. It usually enlarges (dilates) the left \n"
          "ventricle.")

def cause_heartInfection():
    print("Heart Infections: ")
    print("A heart infection, such as endocarditis, is caused when an irritant, such as a bacterium, \n"
          "virus or chemical, reaches your heart muscle. The most common causes of heart infection include:")
    print(" - Bacteria", "Viruses", "Parasites", sep="\n - ")

def cause_valvular():
    print("Valvular heart disease: ")
    print("There are many causes of diseases of your heart valves. You may be born with valvular disease, \n"
          "or the valves may be damaged by conditions such as:")
    print(" - Rheumatic fever", "Infections (infectious endocarditis)", "Connective tissue disorders", sep="\n - ")

def PrintCauses(disease):
    print("\n\n")
    print("********************************************** CAUSES ******************************************************")

    if disease == "atherosclerotic":
        cause_atherosclerotic()
    elif disease == "arrhythmias":
        cause_arrhythmia()
    elif disease == "heart defects less Serious" or disease == "heart defects serious infant" or "heart defects serious child":
        cause_heartDefects()
    elif disease == "dilated cardiomyopathy":
        cause_cardiomyopathy()
    elif disease == "heart infections":
        cause_heartInfection()
    elif disease == "valvular":
        cause_valvular()
    else:
        print("No Cause Found")


####################### DEFINITIONS ####################################

def VarifyInput(userInput):
    if userInput == "":
        return ""
    elif userInput[0] == 'y' or userInput[0] == 'Y':
        return "y"
    elif userInput[0] == 'n' or userInput[0] == 'N':
        return "n"
    else:
        return ""
def TakeInput(message):
    inp = input(message + " (y/n): ")
    inp = VarifyInput(inp)
    while inp == "":
        inp = input(message + " (y/n): ")
        inp = VarifyInput(inp)

    return inp

def setAge():
    print("Select age group below(years): ", "1. infant(0 - 3)", "2. child(3 - 15)", "3. adult(15+)", sep="\n",end="\n")
    while True:
        _inp = input("Enter age-group:")
        if _inp != '1' and _inp != '2' and _inp != '3':
           print("invalid age group")
           _inp = ""
        else:
            if _inp == '1':
                _age = "infant"
            elif _inp == '2':
                _age = "child"
            else:
                _age = "adult"

            print("age-group: ", _age)
            return _age

def VarifyGender(userInput):
    if userInput[0] == 'm' or userInput[0] == 'M':
        return "m"
    elif userInput[0] == 'f' or userInput[0] == 'F':
        return "f"
    else:
        return ""
def setGender():
    print("m -> Male and f -> Female")
    _inp = input("Enter Gender (m/f): ")
    _inp = VarifyGender(_inp)
    while _inp == "":
        _inp = input("Enter Gender (m/f): ")
        _inp = VarifyGender(_inp)

    if _inp == "m":
        _gender = "male"
    elif _inp == "f":
        _gender = "female"
    else:
        _gender = ""
    print("Gender: ", _gender)
    return _gender


################################## PREDICTION RULES ################################################

rules = {
    "atherosclerotic" : "inactive",
    "arrhythmias" : "inactive",
    "heart defects" : "inactive",
    "dilated cardiomyopathy" : "inactive",
    "endocarditis" : "inactive",
    "valvular heart disease" : "inactive"
}
symptoms = []
possible_Diseases = {}
d_count = {
    "atherosclerotic": 0,
    "arrhythmias": 0,
    "heart defects less Serious": 0,
    "heart defects serious infant": 0,
    "heart defects serious child": 0,
    "dilated cardiomyopathy": 0,
    "heart infections": 0,
    "valvular": 0
}



atherosclerotic_male = {
    "chest pain": "",
    "shortness of breath": "",
    "pain numbness weakness coldness in legs or arms": "",
    "pain in neck": ""
}
atherosclerotic_female ={

    "nausea": "",
    "extreme fatigue": "",
    "chest pain": "",
    "shortness of breath": "",
    "pain numbness weakness coldness on legs": "",
    "pain in neck": ""
}
arrhythmias = {
    "fluttering in chest": "",
    "tachycardia": "",
    "bradycardia": "",
    "chest pain": "",
    "lightheadedness": "",
    "dizziness": "",
    "fainting": ""
}
heart_defects_lessSerious = {
    "shortness of breath": "",
    "easily tired": "",
    "swelling in hands ankles feet": ""
}
heart_defects_serious_infant = {
    "shortness of breath during feeding": "",
    "poor weight gain": ""
}
heart_defects_serious_child  = {
    "pale gray or blue skin color": "",
    "swelling in legs": "",
    "dark circles around eyes": ""
}
dilated_cardiomyopathy = {
    "shortness of breath": "",
    "swelling of legs ankles or feet": "",
    "fatigue": "",
    "irregular heart beats": "",
    "dizziness": "",
    "lightheadedness": "",
    "fainting": ""
}
heart_infections = {
    "fever": "",
    "shortness of breath": "",
    "weakness": "",
    "fatigue": "",
    "change in heart rhythm": "",
    "dry or persistent cough": "",
    "skin rashes or unusual spots": ""
}
valvular = {
    "fatigue": "",
    "shortness of breath": "",
    "irregular heart beats": "",
    "swelling of feet ankles": "",
    "chest pain": "",
    "fainting": ""
}

################################ PREDICTON METHODS ################################################

def Causes():
    _inp = TakeInput("Do you want to show the cause of High possibility disease?")
    if _inp == 'y':
        h = m = 0
        for d in possible_Diseases:
            pri = possible_Diseases[d]
            if pri == "high":
                h += 1
            elif pri == "mid":
                m += 1
        if h > 0:
            p = "high"
        else:
            p = "mid"

        for disease in possible_Diseases:
            pri = possible_Diseases[disease]
            if pri == p:
                PrintCauses(disease)



def PossibleDisease():
    for disease in d_count:
        count = d_count[disease]
        if count > 0:
            pri = chkPriority(count, symptoms.__len__())
            possible_Diseases.update({disease:pri})

def PrintDisHigh():
    for disease in possible_Diseases:
        pri = possible_Diseases[disease]
        if pri == "high":
            print("% 30s | high" % disease)
            #print(f"{disease}\t\t\t high")
def PrintDisMed():
    for disease in possible_Diseases:
        pri = possible_Diseases[disease]
        if pri == "mid":
            print("% 30s | mid" % disease)
            # print(f"{disease}\t\t\t mid")
def PrintDisLow():
    for disease in possible_Diseases:
        pri = possible_Diseases[disease]
        if pri == "low":
            print("% 30s | low" % disease)
            # print(f"{disease}\t\t\t low")

def updatedcount(sypt):
    if sypt in arrhythmias:
        count = d_count["arrhythmias"]
        d_count.update({"arrhythmias": count + 1})
    if sypt in atherosclerotic_male or sypt in atherosclerotic_female:
        count = d_count["atherosclerotic"]
        d_count.update({"atherosclerotic": count + 1})
    if sypt in heart_defects_lessSerious:
        count = d_count["heart defects less Serious"]
        d_count.update({"heart defects less Serious": count + 1})
    if sypt in heart_defects_serious_child:
        count = d_count["heart defects serious child"]
        d_count.update({"heart defects serious child": count + 1})
    if sypt in heart_defects_serious_infant:
        count = d_count["heart defects serious infant"]
        d_count.update({"heart defects serious infant": count + 1})
    if sypt in dilated_cardiomyopathy:
        count = d_count["dilated cardiomyopathy"]
        d_count.update({"dilated cardiomyopathy": count + 1})
    if sypt in heart_infections:
        count = d_count["heart infections"]
        d_count.update({"heart infections": count + 1})
    if sypt in valvular:
        count = d_count["valvular"]
        d_count.update({"valvular": count + 1})

def predDisease():
    print("\n", "\n",
          "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
    print("predicting Disease...")
    for sypt in symptoms:
        updatedcount(sypt)

    PossibleDisease()
    PrintDisHigh()
    PrintDisMed()
    PrintDisLow()
    Causes()
    # print(possible_Diseases)

def chkPriority(dcount, tot_sypt):
    res = (dcount/tot_sypt)*100
    if res > 70:
        return "high"
    elif res > 50:
        return "mid"
    else:
        return "low"


################################## MAIN #############################################
print()
print("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
print("|||||||||||||||||||||||||||||||||||||||||||||| HEART DISEASE PREDICTOR |||||||||||||||||||||||||||||||||||||||||")
print("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")

print("---------------------------------------------------AGE----------------------------------------------------------")
_age = setAge()
print("---------------------------------------------------GENDER-------------------------------------------------------")
_gender = setGender()


_inp = TakeInput("do you have chest pain?")
if _inp == 'y':
    symptoms.append("chest pain")
    if _gender == "female":
        _inp = TakeInput("Do you have nausea?")
        if _inp == 'y':
            symptoms.append("nausea")
        _inp = TakeInput("Do you have extreme fatigue?")
        if _inp == 'y':
            symptoms.append("extreme fatigue")

    _inp = TakeInput("do you have pain, numbness, weakness or coldness in legs or arms?")
    if _inp == 'y':
        symptoms.append("pain numbness weakness coldness in legs or arms")
        _inp = TakeInput("do you feel pain in neck?")
        if _inp == 'y':
            symptoms.append("pain in neck")
    else:
        _inp = TakeInput("Do you feel Dizziness?")
        if _inp == 'y':
            symptoms.append("dizziness")
            _inp = TakeInput("Do you feel slow heart beats?")
            if _inp == 'y':
                symptoms.append("bradycardia")
            else:
                _inp = TakeInput("Do you feel racing heart beats?")
                if _inp == 'y':
                    symptoms.append("tachycardia")
            _inp = TakeInput("do you feel fainting?")
            if _inp == 'y':
                symptoms.append("fainting")
        else:
            _inp = TakeInput("do you feel fatigue?")
            if _inp == 'y':
                symptoms.append("fatigue")
                _inp = TakeInput("do you feel Irregular heart beats?")
                if _inp == 'y':
                    symptoms.append("fainting")
                    _inp = TakeInput("do you feel problem in breathing?")
                    if _inp == 'y':
                        symptoms.append("short of breath")


else:
    _inp = TakeInput("Do you have any family background in heart disease?")
    if _inp == 'y':
            if _age == "infant":
                _inp = TakeInput("Does the child have problem breathing while feeding?")
                if _inp == 'y':
                    symptoms.append("shortness of breath during feeding")
                    _inp = TakeInput("Does the child is leading to poor weight gain?")
                    if _inp == 'y':
                        symptoms.append("poor weight gain")
            if _age == "adult" or _age == "child":
                _inp = TakeInput("Do you have Swelling in hands or ankles or feet?")
                if _inp == 'y':
                    symptoms.append("swelling in hands ankles feet")
                _inp = TakeInput("Do you easily get out of breath during exercises or activity?")
                if _inp == 'y':
                    symptoms.append("shortness of breath")
                    _inp = TakeInput("Do you easily get tired during exercise or acitvity?")
                    if _inp == 'y':
                        symptoms.append("easily tired")
                else:
                    if _age == "child":
                        _inp = TakeInput("Do you have swelling in legs")
                        if _inp == 'y':
                            symptoms.append("swelling in legs")
                            _inp = TakeInput("Do you have dark circles around eyes")
                            if _inp == 'y':
                                symptoms.append("dark circles around eyes")
                        _inp = TakeInput("Pale grey or blue skin color?")
                        if _inp == 'y':
                            symptoms.append("pale gray or blue skin color")

    else:
        _inp = TakeInput("do you feel problem in breathing?")
        if _inp == 'y':
            symptoms.append("short of breath")
        _inp = TakeInput("do you have swelling in legs, ankles or feet?")
        if _inp == 'y':
            symptoms.append("swelling of legs ankles or feet")
        _inp = TakeInput("do you feel Irregular heart beats?")
        if _inp == 'y':
            symptoms.append("irregular heart beats")
            _inp = TakeInput("do you feel Dizziness?")
            if _inp == 'y':
                symptoms.append("dizziness")
                _inp = TakeInput("do you feel lightheadedness?")
                if _inp == 'y':
                    symptoms.append("lightheadedness")
                _inp = TakeInput("do you feel like fainting?")
                if _inp == 'y':
                    symptoms.append("fainting")
        else:
            _inp = TakeInput("Do you have dy or persistent cough?")
            if _inp == 'y':
                symptoms.append("dry or persistent cough")
            _inp = TakeInput("Do you feel changes in heart rhythm?")
            if _inp == 'y':
                symptoms.append("change in heart rhythm")
            _inp = TakeInput("Do you have skin rashes or unusual spots?")
            if _inp == 'y':
                symptoms.append("skin rashes or unusual spots")


predDisease()